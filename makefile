CC = g++
CFLAG = -g -Wall -std=c++11

TARGET=CPE381_RTSP_Phase2

all: $(TARGET)

$(TARGET) : $(TARGET).cpp
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).cpp

clean:
	$(RM) $(TARGET)
