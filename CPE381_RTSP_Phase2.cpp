/*
 * File name: CPE381_RTSP_Phase2.cpp
 * Brief: Parses a modfied .wav file and filters frequencies above 1600Hz
 *      to simulate the removal of noise
 * Author: Chandler Ellis - cge0005@uah.edu
 * Date 04/18/2018
 * Input: ellis_c_mod.wav
 * Output: ellis_c_lp.wav
 *
 * Compilation note: must compile with -std=c++11, flag is included in
 *      accompanying makefile
 */


#include <iostream>
#include <cstdint> //for uintX_t types
#include <ctime>
#include <cmath>
#include <fstream>
#include "fdacoefs.h"


using namespace std;

//
// !!! Type definitions !!!
//

enum channel_state {
    Mono = 1,
    Stereo = 2
};

struct wav_header_t {
    uint8_t chunkID[4]; // contains "RIFF" in ascii
    uint32_t chunkSize; // size of the chunk following this number
    uint8_t format[4];  // contains the letters "WAVE"
    uint8_t subChunkID[4]; // contains the letters "fmt "
    uint32_t subChunkSize;
    uint16_t audioFormat; // anything other than 1 signifies compression
    uint16_t numChannels; // mono = 1, stereo = 2
    uint32_t sampleRate; // the sample rate of the audio signal
    uint32_t byteRate; // sampleRate * numChannels * Bits per sample/ 8
    uint16_t blockAlign; // number of bytes for one sample including all numChannels
    uint16_t bitsPerSample; // 8 or 16 bits
};

struct chunk_t {
    uint8_t dataChunkID[4]; // "data" in big edian
    uint32_t dataChunkSize; // numspamples * num channels * bitspersample/8
};


//
// !!! Prototype Definitions !!!
//

void filter_noisy_signal(const char*, const char*);

int16_t FIRFilter(int16_t*, int16_t, int, const double*);

int8_t FIRFilter(int8_t*, int8_t, int, const double*);



/*
 * !!! Main !!!
 */
int main(int argc, char const **argv)
{
    clock_t t = clock(); //time consumed by program

    cout << "Welcome!" << endl;

    filter_noisy_signal("ellis_c_mod.wav", "ellis_c_lp.wav");

    t = clock() - t; //compute number of clock ticks since initial clock

    float t_seconds = t/CLOCKS_PER_SEC; // compute number of seconds since initial clock

    cout << "Program complete, ellis_c_lp.wav has been generated" << endl;
    cout << "Program completed in " << t_seconds << " second(s)" << endl;
    return 0;
}


/*
 * Brief: Function filters out certain nosie with an FIR filter
 *      and writes the resulting audio to a .wav file
 *
 * Inputs: input/output filenames
 *
 */
void filter_noisy_signal(const char* input_filename, const char* output_filename)
{
    FILE* ifp; //input file pointer
    FILE* ofp; //output file pointer

    ifp = fopen(input_filename, "rb");

    if(!ifp)
    {
        cout << "Error opening input file: " << input_filename << "file could not be opened" << endl;
        cout << "Exiting..." << endl;
        return;
    }
    else
    {
        cout << "Input file: " << input_filename << " opened successfully" << endl;
    }

    ofp = fopen(output_filename, "w");

    if(!ofp)
    {
        cout << "Error opening output file: " << output_filename << "file could not be opened" << endl;
    }
    else
    {
        cout << "Output file: " << output_filename << " opened successfully" << endl;
    }

    struct wav_header_t header;

    fread(&header, sizeof(header), 1, ifp);

    chunk_t data_chunk; // data chunk information struct

    fwrite(&header, sizeof(header), 1, ofp); // write header info

    // find data chunk 0x61746164 = "data" big edian
    do
    {
        fread(&data_chunk, sizeof(data_chunk), 1, ifp);
    }while(*(uint32_t *)&data_chunk.dataChunkID != 0x61746164);


    fwrite(&data_chunk, sizeof(data_chunk), 1, ofp); // write data chunk

    int num_samples = data_chunk.dataChunkSize * 8 / (header.bitsPerSample * header.numChannels);

    int sample_size = header.bitsPerSample / 8; // size in bytes of a sample i.e. mono -1 stereo -2

    cout << "Starting filtering operations..." << endl;

    int filter_length = 0;
    const double* p_coeff; //pointer to b const coefficent array

    if(header.sampleRate == 44100)
    {
        filter_length = FILTER_LEN;
        p_coeff = B;
    }
    else if(header.sampleRate == 48000)
    {
        filter_length = FILTER_LEN;
        p_coeff = B1;
    }
    //following instructions determine mono or stereo and process accordingly, TODO: consolidate this into a more generic implementation

    if(sample_size == Mono)
    {
        int8_t* input_data = new int8_t[FILTER_LEN]; //dynamically allocate sound array

        int8_t current_sample = 0;
        int8_t filtered_sample = 0;

        while(!feof(ifp))
        {
            fread(&current_sample, sample_size, 1, ifp);

            filtered_sample = FIRFilter(input_data, current_sample, filter_length, p_coeff);

            fwrite(&filtered_sample, sizeof(int8_t), 1, ofp);
        }

        delete[] input_data;
    }
    else if(sample_size == Stereo)
    {
        int16_t* input_data = new int16_t[FILTER_LEN];

        int16_t current_sample = 0;
        int16_t filtered_sample = 0;

        while(!feof(ifp))
        {
            fread(&current_sample, sample_size, 1, ifp); //get next sample in file

            filtered_sample = FIRFilter(input_data, current_sample, filter_length, p_coeff); // filter the input

            fwrite(&filtered_sample, sizeof(int16_t), 1, ofp); //write the filtered result to the output file
        }

        delete[] input_data;
    }
}



/*
 * Brief: Function implements a basic FIR Filter
 * Input: input_data buffer and sample being processed
 *
 * NOTE: This is an overloaded function!
 *
 * TODO: Implement a faster method (ring buffer) if time permits
 */
int16_t FIRFilter(int16_t* input_signal, int16_t current_sample, int filter_length, const double* p_coeff)
{
    int16_t summation;

    for(int i = filter_length-1; i > 0; i--)
    {
        input_signal[i] = input_signal[i-1];
    }

    input_signal[0] = current_sample;

    for(int i = 0; i < filter_length; i++)
    {
        summation += input_signal[i]*p_coeff[i]; //perform convolution
    }

    return summation;

}

/*
 * Brief: Implements a basic FIR filter
 * Input: input_data buffer and sample being processed
 *
 * NOTE: This is an overloaded function!
 */
int8_t FIRFilter(int8_t* input_signal, int8_t current_sample, int filter_length, const double* p_coeff)
{
    int8_t summation;

    for(int i = filter_length-1; i > 0; i--)
    {
        input_signal[i] = input_signal[i-1];
    }

    input_signal[0] = current_sample;

    for(int i = 0; i < filter_length; i++)
    {
        summation += input_signal[i]*p_coeff[i]; //perform convolution
    }

    return summation;

}



